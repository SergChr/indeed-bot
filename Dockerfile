FROM node:12

RUN mkdir -p /app
WORKDIR /app

ADD . /app

# setup
RUN npm ci

# build
RUN npm run build

EXPOSE 8001

# run built app
CMD npm run start
