import Agenda from 'agenda'
import axios from 'axios'
import cheerio from 'cheerio'
import _ from 'lodash'

import { Keyword } from '@database'
import { sendNewJobs } from '@services/telegram'
import { IndeedJob } from '@types'
import userAgents from '@data/userAgents'

const config = {
  headers: {
    'User-Agent': userAgents[getRandomInt(0, userAgents.length - 1)],
    'Sec-Fetch-Mode': 'cors',
  }
}

export default async (job: Agenda.Job) => {
  const keywords = await Keyword.find().populate('users')
  keywords.forEach(async ({ name, lastPostIds, _id: keywordId, users }) => {
    try {
      const { data } = await axios.get(getURL(name), config)
      const $ = cheerio.load(data)
      const html = $('.jobsearch-SerpJobCard')
      const divs = Object.values(html).filter(object => object.type && object.name)
      const jobs: IndeedJob[] = divs.map(div => ({
        id: $(div).attr('id'),
        title: $(div).find('.title').find('a').text().trim(),
        link: `https://indeed.com${$(div).find('a').attr('href')}`,
        location: $(div).find('.sjcl').find('.location').text().trim(),
        company: $(div).find('.sjcl').find('div').find('.company').find('a').text().trim(),
      }))
      const ids = divs.map(div => $(div).attr('id'))
      const diff = _.differenceWith(ids, lastPostIds.split(','), _.isEqual)

      if (diff.length) {
        const existingKeywords = await Keyword.findByIdAndUpdate(keywordId, { lastPostIds: ids.join(',') })
        const lastPostIds = _.get(existingKeywords, 'lastPostIds', '')
        // Don't send first fetched jobs
        if (lastPostIds === '') {
          return
        }
        const telegramUsersIds = users.map(user => user.telegramId)
        const newJobs = jobs.filter(job => diff.some(id => job.id === id))
        await sendNewJobs(newJobs, telegramUsersIds)
      }
    } catch (e) {
      console.error(e)
    }
  })
}

function getURL (name: string) {
  const [jobName, location] = name.split(';')

  return `https://indeed.com/jobs?as_phr=${jobName}&jt=all&radius=25&l=${location}&fromage=3&limit=5&sort=date&psf=advsearch`
}

function getRandomInt (min: number, max: number) {
  min = Math.ceil(min)
  max = Math.floor(max)

  return Math.floor(Math.random() * (max - min + 1)) + min
}
