import Agenda from 'agenda'

import { MONGODB } from '@config'
import fetchJobs from './fetchJobs'

export type Done = (err?: Error | undefined) => void

const connectionString = `mongodb://${MONGODB.USER}:${MONGODB.PASSWORD}@${MONGODB.URI}/${MONGODB.DB_NAME}`
const agenda = new Agenda({
  db: {
    address: connectionString,
    options: { useNewUrlParser: true },
  },
  processEvery: '5 seconds',
})

agenda.define('fetch jobs', fetchJobs)

export default agenda
