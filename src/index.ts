import express from 'express'
import expressPino from 'express-pino-logger'
import mongoose from 'mongoose'

import { connect as connectToDatabase } from '@database'
import { MONGODB, COMMON } from '@config'
import log from '@helpers/logger'
import TelegramBot from '@services/telegram'
import agenda from '@jobs'

const server = express()

server.use(expressPino)

connectToDatabase({
  user: MONGODB.USER, 
  password: MONGODB.PASSWORD, 
  db: MONGODB.DB_NAME, 
  uri: MONGODB.URI,
}).then(() => {
  server.listen(COMMON.PORT, async () => {
    log.info('Server started.')
    TelegramBot.launch()
    agenda.start()

    // Clean agendaJobs collection on each server start (agenda can't start properly sometimes)
    await mongoose.connection.db.dropCollection('agendaJobs')
    await agenda.every('3 minutes', 'fetch jobs')
  })
}).catch(e => console.error('Can\'t start a server', e))
