import * as mongoose from 'mongoose'

const { Schema, model } = mongoose

export interface User extends mongoose.Document {
  _id: mongoose.Schema.Types.ObjectId;
  firstName: string;
  telegramId: number;
  isBot: boolean;
  username: string;
  languageCode: string;
}

const User = new Schema({
  firstName: { type: String },
  telegramId: { type: Number, required: true, unique: true },
  isBot: { type: Boolean },
  username: { type: String },
  languageCode: { type: String },
}, { timestamps: true })

export default model<User>('User', User)
