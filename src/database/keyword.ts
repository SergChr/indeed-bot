import * as mongoose from 'mongoose'

import { User } from './user'

const { Schema, model } = mongoose

export interface Keyword extends mongoose.Document {
  _id: mongoose.Schema.Types.ObjectId;
  name: string;
  users: User[];
  lastPostIds: string;
}

const Keyword = new Schema({
  name: { type: String, unique: true, required: true },
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  lastPostIds: { type: String, default: '' },
}, { timestamps: true })

export default model<Keyword>('Keyword', Keyword)
