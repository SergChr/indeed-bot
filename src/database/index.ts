import mongoose from 'mongoose'

import User from './user'
import Keyword from './keyword'

type connectArgs = {
  user: string
  password: string
  uri: string
  db: string
}

const options = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
}

export const connect = ({ user, password, uri, db }: connectArgs) =>
  mongoose.connect(`mongodb://${user}:${password}@${uri}/${db}`, options)

export {
  User,
  Keyword,
}
