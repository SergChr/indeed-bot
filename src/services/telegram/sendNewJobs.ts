import { IndeedJob } from '@types'
import { Telegram } from './index'

type Job = {
  title: string;
  link: string;
  company: string;
  location: string;
}

const newJobText = ({ title, link, company, location }: Job) =>`
🧰 [${title}](${link})
Company: ${company || 'unknown'}
Where: ${location}
`

export default async function sendNewJobs (jobs: IndeedJob[], userIds: number[]) {
  try {
    userIds.forEach(telegramId => {
      const text = jobs.map(newJobText)
      return Telegram.sendMessage(telegramId, text.join('\n'), { parse_mode: 'Markdown' })
    })
  } catch(e) {
    console.error(e)
  }
}
