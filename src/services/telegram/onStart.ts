import { User } from '@database'
import log from '@helpers/logger'

const welcomeText = `
Hi! I can help you to subscribe to USA Indeed jobs. I will notify you when a new job posted by a particular keyword.
/add adds a keyword to subscribe, in format "<job title>;<location>" e.g. "/add sales manager; London"
/remove shows a list of your keyword you can remove.

If you need help, write to @mgyoda (Telegram).
`

export default async function onStart (ctx: any) {
  const { message: { from } } = ctx.update
  let user = await User.findOne({ telegramId: from.id })
  if (!user) {
    user = await User.create({
      telegramId: from.id,
      firstName: from.first_name,
      isBot: from.is_bot,
      username: from.username,
      languageCode: from.language_code,
    })
    log.info(`New user added: ${from.username}`)
  }

  ctx.session.user = user
  return ctx.reply(welcomeText)
}
