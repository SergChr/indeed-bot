import Telegraf, { Telegram as TelegrafT } from 'telegraf'
import session from 'telegraf/session'
import _ from 'lodash'

import onKeywordAdd from './onKeywordAdd'
import onStart from './onStart'
import sendNewJobs from './sendNewJobs'
import removeKeyword from './removeKeyword'

const bot = new Telegraf('943296880:AAF6l9Ls7ecdDtu8Hh64NVs2M2zqlRGkvMY')
export const Telegram = new TelegrafT('943296880:AAF6l9Ls7ecdDtu8Hh64NVs2M2zqlRGkvMY')

bot.use(session())
bot.start(onStart)
bot.command('add', onKeywordAdd)
bot.command('remove', removeKeyword)

bot.catch((err: any) => {
  console.error('Telegram bot error:', err)
})

export {
  sendNewJobs,
}

export default bot
