import _ from 'lodash'
import Markup from 'telegraf/markup'

import { User, Keyword } from '@database'

export default async function removeKeyword (ctx: any) {
  const message = ctx.update.message

  let { user } = ctx.session
  if (!user) {
    user = await User.findOne({ telegramId: message.from.id })
    ctx.session.user = user
  }

  if (user) {
    const keywords = await Keyword.find({ users: { $in: [user._id] } })
    const keywordName = message.text.replace('/remove', '').trim()
    const keyword = await Keyword.findOne({ name: keywordName })
    if (keywordName === '' || !keyword) {
      return ctx.reply('Keywords you can remove',
        Markup.keyboard(keywords.map(({ name }) => `/remove ${name}`))
          .oneTime()
          .resize()
          .extra()
      )
    } else {
      const unsubscribedKeyword = await Keyword.findOneAndUpdate(
        { name: keywordName },
        { $pull: { users: user._id } }
      )
      return ctx.reply(`You\'ve unsubscribed from "${_.get(unsubscribedKeyword, 'name')}" successfully!`)
    }
  }
}
