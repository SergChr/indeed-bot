import _ from 'lodash'

import log from '@helpers/logger'
import { User, Keyword } from '@database'

export default async function onKeywordAdd (ctx: any) {
  const message = ctx.update.message
  const keywordName = message.text.replace('/add', '').trim()

  const user = await User.findOne({ telegramId: message.from.id })
  const keyword = await Keyword.findOne({ name: keywordName })

  if (keyword) {
    await keyword.update({ $addToSet: { users: _.get(user, '_id') } })
  }

  if (!keyword) {
    const newKeyword = await Keyword.create({
      name: keywordName,
      users: [_.get(user, '_id')],
    })
    log.info(`New keyword added: ${newKeyword.name}`)
    ctx.reply('Got it. I\'ll send you new jobs once they are posted.')
  }
}
