import { User } from '@database/user'

export type IndeedJob = {
  id: string;
  title: string;
  link: string;
  location: string;
  company: string;
}

type Message = {
  from: {
    id: number;
    first_name: string;
    last_name?: string;
    username: string;
    is_bot: boolean;
    language_code: string;
  },
  text: string;
}

export type Context = {
  session: {
    user: User;
  },
  update: {
    message: Message;
  }
  message: Message;
  reply: (message: string) => void;
}
