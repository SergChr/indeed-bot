const ENV = process.env.NODE_ENV

export const MONGODB = {
  USER: 'fablebee', 
  PASSWORD: 'o3i8^&trl3foO#feE2#2', 
  DB_NAME: 'indeed', 
  URI: `${ENV === 'production' ? 'db' : 'localhost'}:27017`
}

export const COMMON = {
  PORT: 8001,
}
